#!!! CECI EST LA SOUTENANCE !!!   
    #FROM ubuntu:latest
    #RUN apt update
    #RUN apt install apache2 -y
    #RUN apt install apache2-utils -y
    #RUN apt clean
    #EXPOSE 90
    #CMD [“apache2ctl”, “-D”, “FOREGROUND”]
    #COPY /var/www/html/test_cicd/* /var/www/html/test_cicd/


# Utiliser l'image officielle Ubuntu comme base
FROM ubuntu:latest

# Mettre à jour les paquets et installer Apache
RUN apt-get update && \
    apt-get install -y apache2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Exposer le port 80 pour le trafic web
EXPOSE 80

# Définir le répertoire de travail
WORKDIR /var/www/html

# Copier le contenu de votre application dans le conteneur
#COPY ./test_cicd/Dockerfile /var/www/html
#COPY ./test_cicd/check.sh /var/www/html
#COPY ./test_cicd/conteneur.sh /var/www/html
#COPY ./test_cicd/README.md /var/www/html
#COPY ./test_cicd/hello.html /var/www/html
#COPY ./test_cicd/paire_key.pem /var/www/html

COPY . /var/www/html

# CMD est utilisé pour définir la commande par défaut à exécuter lors du démarrage du conteneur
CMD ["apache2ctl", "-D", "FOREGROUND"]


