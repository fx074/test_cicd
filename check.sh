#!/bin/bash
SITE_URL="http://13.37.106.27"
if curl --head --fail $SITE_URL; then
  echo "Le site est en ligne."
  exit 0
else
  echo "Le site est hors ligne."
  exit 1
fi
